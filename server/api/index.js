import { Router } from 'express'

const router = Router()

router.post('/welp/v1/lead-ad/score', (req, res) => {
  res.json({
    "id": "5bdc69fc7fc1db3241db90d6",
    "identification": {
      "type": "IC",
      "value": "12345678"
    },
    "gender": "F",
    "brand": "WELP",
    "country": "AR",
    "source": "Askrobin",
    "bank_code": "150",
    "birth_date": "1900-01-01",
    "created_date": "2018-11-02T12:15:08.057"}
  )
})
router.post('/welp/v1/lead-ad', (req, res) => {
  res.json({
    "lead_id" : "5cdf4fd74cedfd00016d9f75",
    "operation_id" : "5cdf4e2dc34822001747e4b9",
    "creation_date" : "2019-05-18T00:20:39",
    "redirect_url" : "https://app.welp.com/?formLead=5cdf4fd74cedfd00016d9f75&utm_source=AskRobin&utm_medium=afiliados&utm_campaign=conversion&source=AskRobin&medium=afiliados&adGroup=welpar"
    }
  )
})

router.post('/mango/v1/lead-ad/score', (req, res) => {
  res.json({
    "id": "5bdc69fc7fc1db3241db90d6",
    "identification": {
      "type": "IC",
      "value": "12345678"
    },
    "gender": "M",
    "brand": "MANGO",
    "country": "AR",
    "source": "Askrobin",
    "bank_code": "150",
    "birth_date": "1900-01-01",
    "created_date": "2018-11-02T12:15:08.057"}
  )
})
router.post('/mango/v1/lead-ad', (req, res) => {
  res.json({
    "lead_id" : "5ce8afc94cedfd00019ffc3a",
    "operation_id" : "5ce8aea39a34dc0017dd1f0d",
    "creation_date" : "2019-05-25T03:00:25",
    "redirect_url" : "https://app.holamango.com/?formLead=5ce8afc94cedfd00019ffc3a&utm_source=AskRobin&utm_medium=afiliados&utm_campaign=conversion&source=AskRobin&medium=afiliados&adGroup=mangoar"
  })
})

router.post('/posta/v1/lead-ad/score', (req, res) => {
  res.json({
    "id": "5bdc69fc7fc1db3241db90d6",
    "identification": {
      "type": "IC",
      "value": "12345678"
    },
    "gender": "M",
    "brand": "POSTA",
    "country": "AR",
    "source": "Askrobin",
    "bank_code": "150",
    "birth_date": "1900-01-01",
    "created_date": "2018-11-02T12:15:08.057"}
  )
})
router.post('/posta/v1/lead-ad', (req, res) => {
  res.json({
    "lead_id" : "5ce8afc94cedfd00019ffc3a",
    "operation_id" : "5ce8aea39a34dc0017dd1f0d",
    "creation_date" : "2019-05-25T03:00:25",
    "redirect_url" : "https://app.holamango.com/?formLead=5ce8afc94cedfd00019ffc3a&utm_source=AskRobin&utm_medium=afiliados&utm_campaign=conversion&source=AskRobin&medium=afiliados&adGroup=mangoar"
  })
})

router.post('/waynimovil/token/creditrisk', (req, res) => {
  res.json({
    status: 'success',
    model:
      { hash: 'mdXs9DvioTjHF5RH',
        first_name: 'Matías',
        last_name: 'Marasca',
        gender: 'M',
        birthdate: '1985-03-30',
        email: 'oliver.pruel@askrobin.com',
        phone: '111234567',
        country_id: 32,
        amount: 0,
        document_type: 1,
        document_number: '29105423',
        origin: 'ask-robin',
        external_id: '5cd9448c5475813c9af9da37',
        suitable: 1,
        is_account: 0,
        link:
        'https://www.waynimovil.com/iniciar/#/vendors/mdXs9DvioTjHF5RH?utm_source=ask-robin&utm_medium=api&utm_campaign=smart_form_new',
        finish:
        'https://www.waynimovil.com/iniciar/#/vendors/mdXs9DvioTjHF5RH?utm_source=ask-robin&utm_medium=api&utm_campaign=smart_form_new'
      }
    })
})

router.post('/vivus/affiliate-applications', (req, res) => {
  res.json({ "token": "wzXvdQKTZ5GjHSMjKxyrRwFQ3vgRcUQsLnpom0Uq", "clientNumber": "9635899", "email": "oliverpruel@askrobin.com", "clientId": 363092, "status": "REGISTERED", "_links": { "self": { "href": "https://www.vivus.com.ar/api/affiliate-applications" }}})
})

export default router
